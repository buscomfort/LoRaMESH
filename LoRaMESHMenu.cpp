#include "LoRaMESHMenu.h"


static HardwareSerial *logger;
static LoRaMESH *localLM;

void header()
{
	logger->println(F("\n\nLoRaMESH TERMINAL CONFIG"));
	logger->println(F("->LOCAL DEVICE INFO"));
	logger->print(localLM->GetLocalDeviceInfoString(true));
	logger->println(F("-->MODULATION INFO"));
	logger->print(localLM->GetLocalDeviceModInfoString(true));
	logger->println(F("_________________"));
}


void mainMenu(HardwareSerial *loggerSerial, LoRaMESH *loraMesh)
{
    logger = loggerSerial;
    localLM = loraMesh;
	char option = 0;
	do
	{	
		header();
		logger->println(F("\nMAIN MENU"));
		logger->println(F("[0] Direct access interfaces"));
		logger->println(F("[1] General device config"));
		logger->println(F("[2] LoRa device config"));
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '2');
		switch (option)
		{
			case '0':
				interfaceMenu();
				break;
			case '1':
				configMenu();
				break;
			case '2':
				loraMenu();
				break;
			default:
				break;
		}
	} while(true);
}

void interfaceMenu(void)
{
	char option = 0;	
	do 
	{
		logger->println(F("\nSelect the interface:"));
		logger->println(F("[0] Return"));	
		logger->println(F("[1] Command Interface"));
		logger->println(F("[2] Transparent Interface"));
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '2');
		switch (option)
		{
			case '0':
				break;
			case '1':
				commandInterface();
				break;
			case '2':
				transparentInterface();
				break;
			default:
				break;
		}
	} while(option != '0');
}

void commandInterface()
{
	char option = 0;
	char msg = ' ';
	String toSend = "";
	do 
	{
		header();
		logger->println(F("\nCOMMAND INTERFACE"));
		logger->println(F("[0] Return"));
		logger->println(F("[1] Send bytes (HEX)"));
		logger->println(F("[2] Listen directly to Command Serial Interface"));	
		logger->println(F("[3] Receive packets"));	
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '3');
		switch (option)
		{
			case '0':
				break;
			case '1':
				logger->println(F("HEX CODE to send:"));
				toSend = "";
				do 
				{	//0000C100FF0000
					while(!logger->available());
					msg = (char) logger->read();
					toSend += msg;
				} while (msg != '\r' && msg != '\n');
				logger->println(F("Message read:"));
				logger->println(toSend);
				logger->println(F("SENDING COMMAND"));
				logger->print(F("SEND STATUS: "));
				logger->println(localLM->SendDirectCommand(toSend));
					
				logger->print(F("RECEIVE STATUS: "));
				logger->println(localLM->ReceivePacketCommand(5000));
				logger->print(F("FRAME SIZE: "));
				logger->println(localLM->frame.size);
				for (int c = 0; c < localLM->frame.size; c++) 
				{
					logger->print(String(localLM->frame.buffer[c], HEX));
					logger->print(' ');
				}
				break;
			case '2':

				break;
			case '9':

				break;
			default:
				break;
		}
	} while(option != '0');
}

void transparentInterface()
{
	char option = 0;
	char msg = ' ';
	String toSend = "";
	do 
	{
		header();
		logger->println(F("\nTRANSPARENT INTERFACE"));
		logger->println(F("[0] Return"));
		logger->println(F("[1] Send bytes (HEX)"));
		logger->println(F("[2] Listen directly to Transparent Serial Interface"));	
		logger->println(F("[3] Receive packets"));	
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '3');
		switch (option)
		{
			case '0':
				break;
			case '1':
				logger->println(F("HEX CODE to send:"));
				toSend = "";
				do 
				{
					while(!logger->available());
					msg = (char) logger->read();
					toSend += msg;
				} while (msg != '\r' && msg != '\n');
				logger->println(F("Message read:"));
				logger->println(toSend);
				logger->println(F("SENDING COMMAND"));
				logger->print(F("SEND STATUS: "));
				logger->println(localLM->SendDirectTransparent(toSend));
					
				logger->print(F("RECEIVE STATUS: "));
				logger->println(localLM->ReceivePacketTransp(5000));
				logger->print(F("FRAME SIZE: "));
				logger->println(localLM->frame.size);
				for (int c = 0; c < localLM->frame.size; c++) 
				{
					logger->print(String(localLM->frame.buffer[c], HEX));
					logger->print(' ');
				}
				break;
			case '2':
				break;
			case '3':
				if (localLM->ReceivePacketTransp(5000) != MESH_ERROR)
				{
					logger->println(F("REMOTE DEVICE INFO"));
					logger->print(localLM->GetRemoteDeviceInfoString());
					logger->print(F("Frame size: "));
					logger->println(localLM->frame.size);
					for (int c = 0; c < localLM->frame.size; c++)
						logger->print(localLM->frame.buffer[c], HEX);
					logger->print('\n');
				}
				break;
			case '9':

				break;
			default:
				break;
		}
	} while(option != '0');
}


void localReadMenu()
{
    uint16_t localId;
	if (localLM->LocalRead(&localId, NULL) != MESH_OK)
		logger->print(F("Couldn't read local ID\n"));
	else if (localLM->ReadModParam(0, NULL) != MESH_OK)
		logger->print(F("Couldn't read modulation params\n"));
	else
		logger->print(F("Local Read sucessful. Menu updated.\n"));

	// if (localId == 0) /* Is a master */
	// {
	// 	logger->print(F("Master\n"));
	// }

	// else /* Is a slave */
	// {
	// 	logger->print(F("Slave\n"));
	// 	delay(500);
	// }
}

void writeLoRaParamMenu()
{
	char option = 0;
	char msg = ' ';
	uint16_t id = 0;
	LoRaMESH_ModulationInfo modInfo;
	String toSend = "";
	do
	{	
		header();
		logger->println(F("\nSelect what to config:"));
		logger->println(F("[0] Return"));	
		logger->println(F("[1] Write LoRa Parameters to current Local Device"));
		logger->println(F("[2] Write LoRa Parameters to current Remote Device"));
		logger->println(F("[3] Write LoRa Parameters to any device (ID needs to be informed)"));
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '3');

		if (option != '0')
		{
			logger->println(F("Power (0 < power < 20):"));
			toSend = "";
			do 
			{
				while(!logger->available());
				msg = (char) logger->read();
				toSend += msg;
			} while (msg != '\r' && msg != '\n');
			logger->print(F("Message read: "));
			logger->println(toSend);
			modInfo.power = (uint8_t) toSend.toInt();
			
			logger->println(F("Bandwidth [0 (125 kHz), 1 (250 kHz) or 2 (500 kHZ)]:"));
			toSend = "";
			do 
			{
				while(!logger->available());
				msg = (char) logger->read();
				toSend += msg;
			} while (msg != '\r' && msg != '\n');
			logger->print(F("Message read: "));
			logger->println(toSend);
			modInfo.bandwidth = (uint8_t) toSend.toInt();

			logger->println(F("Spreading Factor [5 (FSK) or 7 <= sf <=  12 (LoRa)]:"));
			toSend = "";
			do 
			{
				while(!logger->available());
				msg = (char) logger->read();
				toSend += msg;
			} while (msg != '\r' && msg != '\n');
			logger->print(F("Message read: "));
			logger->println(toSend);
			modInfo.spreadingFactor = (uint8_t) toSend.toInt();

			logger->println(F("Coding Rate [1 (4/5), 2 (4/6), 3 (4/7) or 4 (4/8)]:"));
			toSend = "";
			do 
			{
				while(!logger->available());
				msg = (char) logger->read();
				toSend += msg;
			} while (msg != '\r' && msg != '\n');
			logger->print(F("Message read: "));
			logger->println(toSend);
			modInfo.codingRate = (uint8_t) toSend.toInt();
			
			switch (option)
			{
				case '1':
					id = localLM->deviceInfo.id;
					break;
				case '2':
					id = localLM->remoteDeviceInfo.id;
					break;
				case '3':
					logger->println(F("ID of the device as an integer:"));
					toSend = "";
					do 
					{
						while(!logger->available());
						msg = (char) logger->read();
						toSend += msg;
					} while (msg != '\r' && msg != '\n');
					logger->print(F("Message read: "));
					logger->println(toSend);
					id = (uint16_t) toSend.toInt();
					break;
				default:
					break;
			}
			
			logger->println(F("SENDING COMMAND"));
			logger->print(F("SEND STATUS: "));
			logger->println(localLM->WriteModParam(id, modInfo));
					
			// logger->print(F("RECEIVE STATUS: "));
			// logger->println(localLM->ReceivePacketCommand(5000));
			logger->print(F("FRAME SIZE: "));
			logger->println(localLM->frame.size);
			for (int c = 0; c < localLM->frame.size; c++) 
			{
				logger->print(String(localLM->frame.buffer[c], HEX));
				logger->print(' ');
			}
		}
	} while (option != '0');
}

void loraMenu() 
{
	char option = 0;
	char msg = ' ';
	uint16_t id = 0;
	LoRaMESH_ModulationInfo modInfo;
	String toSend = "";
	do
	{	
		header();
		logger->println(F("\nSelect what to config:"));
		logger->println(F("[0] Return"));	
		logger->println(F("[1] Read LoRa Parameters (0xD6)"));
		logger->println(F("[2] Write LoRa Parameters (0xD6)"));
		logger->println(F("[3] Read LoRa Class (0xC1)"));
		logger->println(F("[4] Write LoRa Class (0xC1)"));
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '4');
		switch (option)
		{
			case '0':
				break;
			case '1':
				localReadMenu();
				break;
			case '2':
				writeLoRaParamMenu();
				break;
			case '3':

				break;
			case '4':

				break;
			default:
				break;
		}
	} while(option != '0');
}

void writeConfigMenu() 
{
	char option = 0;
	char msg = 0;
	uint16_t newId = 0;
	uint16_t newNet = 0;
	uint32_t uniqueId = 0;
	String toSend = "";
	do
	{	
		header();
		logger->println(F("\nSelect what to config:"));
		logger->println(F("[0] Return"));	
		logger->println(F("[1] Write Config to current Local Device"));
		logger->println(F("[2] Write Config to current Remote Device"));
		logger->println(F("[3] Write Config to any device (Unique ID needed)"));
		// logger->println(F("[4] Write LoRa Class (0xC1)"));
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '3');
		if (option != '0') 
		{	
			logger->println(F("Type the device new ID (integer < 1024):"));
			toSend = "";
			do 
			{
				while(!logger->available());
				msg = (char) logger->read();
				toSend += msg;
			} while (msg != '\r' && msg != '\n');
			logger->print(F("Message read: "));
			logger->println(toSend);
			newId = (uint16_t) toSend.toInt();
			logger->println(F("Type the device new NET (integer < 1024):"));
			toSend = "";
			do 
			{
				while(!logger->available());
				msg = (char) logger->read();
				toSend += msg;
			} while (msg != '\r' && msg != '\n');
			logger->print(F("Message read: "));
			logger->println(toSend);
			newNet = (uint16_t) toSend.toInt();
			
			switch (option)
			{
				case '1':
					uniqueId = localLM->deviceInfo.uniqueId;
					break;
				case '2':
					uniqueId = localLM->remoteDeviceInfo.uniqueId;
					break;
				case '3':
					logger->println(F("Type the UNIQUE ID of the device as an integer:"));
					toSend = "";
					do 
					{
						while(!logger->available());
						msg = (char) logger->read();
						toSend += msg;
					} while (msg != '\r' && msg != '\n');
					logger->print(F("Message read: "));
					logger->println(toSend);
					uniqueId = (uint32_t) toSend.toInt();
					break;
				default:
					break;
			}

			logger->println(F("SENDING COMMAND"));
			logger->print(F("SEND STATUS: "));
			logger->println(localLM->WriteConfig(uniqueId, newId, newNet, NULL));
					
			// logger->print(F("RECEIVE STATUS: "));
			// logger->println(localLM->ReceivePacketCommand(5000));
			
		}
	} while(option != '0');
}

void configMenu() 
{
	char option = 0;
	do
	{	
		header();
		logger->println(F("\nSelect what to config:"));
		logger->println(F("[0] Return"));	
		logger->println(F("[1] Local Read (0xE2)"));
		logger->println(F("[2] Remote Read (0xD4)"));
		logger->println(F("[3] Write Config (0xCA)"));
		while(!logger->available());
		do 
		{
			option = logger->read();
		} while (option < '0' && option > '3');
		switch (option)
		{
			case '0':
				break;
			case '1':
				localReadMenu();
				break;
			case '2':
				writeConfigMenu();
				break;
			case '3':
				writeConfigMenu();
				break;
			default:
				break;
		}
	} while(option != '0');
}

