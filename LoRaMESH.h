/* ---------------------------------------------------
        Radioenge Equipamentos de Telecomunicações
   ---------------------------------------------------
    This library contains a set of functions to configure
    and operate the EndDevice LoRaMESH Radioenge
  
  Date: 13/12/18
*/
#ifndef LORAMESH_H_
#define LORAMESH_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <Arduino.h>
#include <SomeSerial.h>
#include <HardwareSerial.h>

#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
#include <SoftwareSerial.h>
#endif

/* ----- Defines ------ */

#define MAX_PAYLOAD_SIZE 232
#define MAX_BUFFER_SIZE 237

/* -----  Default Comands List ----- */
typedef enum Cmd_Typedef
{
	CMD_LORAPARAMETER = 	0xD6, 	/* Gets or Sets the LoRa modulation parameters */
	CMD_LORACLASS = 		0xC1,	/* Get or Sets the LoRa Device Class (Class A, B or C) */
	CMD_LOCALREAD = 		0xE2,	/* Gets the ID, NET and UNIQUE ID info from the local device */
	CMD_LOCALNOISE = 		0xE0,	/* Reads the local noise */
	CMD_REMOTERSSI = 		0xE1,	/* Reads the remote RSSI */
	CMD_READSPECTRUM = 		0xE8,	/* Reads the spectrum */
	CMD_REMOTEREAD = 		0xD4,	/* Gets the ID, NET and UNIQUE ID info from a remote device */
	CMD_WRITECONFIG = 		0xCA,	/* Writes configuration info to the device, i.e. NET and ID */
	CMD_SPECIALCONFIG = 	0xCD,	/* Special command, undocummented in datasheet, used to write password with subcommand 0x04 */
	CMD_GPIOCONFIG = 		0xC2,	/* Configures a given GPIO pin to a desired mode, gets or sets its value */
	CMD_DIAGNOSIS = 		0xE7,	/* Gets diagnosis information from the device */
	CMD_READNOISE = 		0xD8,	/* Reads the noise floor on the current channel */
	CMD_READRSSI = 			0xD5,	/* Reads the RSSI between the device and a neighbour */
	CMD_TRACEROUTE = 		0xD2,	/* Traces the hops from the device to the master */
	CMD_SENDTRANSP = 		0x28,	/* Sends a packet to the device's transparent serial port */
	CMD_READTABLE = 		0xE3,	/* Reads a Table */
	CMD_CLEANTABLE = 		0xBE,	/* Cleans a Table */
	CMD_TESTLED = 			0x83,	/* Blink the board LED(s) */
	CMD_OTHERTEST = 		0x88,	/* Send other test commands to a target device */
	CMD_SENDCOMMANDVIAOTHERNODE = 0xEF /* Sends a command via another node */
} Cmd_Typedef;

/* Table Enum */
typedef enum Table_Typedef
{
	GATEWAYTABLE = 0x0000, /*  */
	REPEATTABLE = 0x270F,  /*  */
} Table_Typedef;

/* Config BaudRate Enum */
typedef enum BaudRate_Typedef
{
	BAUDRATE9600 	= 0x00, /*  */
	BAUDRATE38400 	= 0x01,  /*  */
	BAUDRATE57600 	= 0x02,  /*  */
	BAUDRATE115200 	= 0x03,  /*  */
} BaudRate_Typedef;

/* Power Enum */
typedef enum LoRaPower_Typedef
{
	POWER0DBM = 0x00,  /* LoRa Power of 0 dBm */
	POWER1DBM = 0x01,  /* LoRa Power of 1 dBm */
	POWER2DBM = 0x02,  /* LoRa Power of 2 dBm */
	POWER3DBM = 0x03,  /* LoRa Power of 3 dBm */
	POWER4DBM = 0x04,  /* LoRa Power of 4 dBm */
	POWER5DBM = 0x05,  /* LoRa Power of 5 dBm */
	POWER6DBM = 0x06,  /* LoRa Power of 6 dBm */
	POWER7DBM = 0x07,  /* LoRa Power of 7 dBm */
	POWER8DBM = 0x08,  /* LoRa Power of 8 dBm */
	POWER9DBM = 0x09,  /* LoRa Power of 9 dBm */
	POWER10DBM = 0x0A, /* LoRa Power of 10 dBm */
	POWER11DBM = 0x0B, /* LoRa Power of 11 dBm */
	POWER12DBM = 0x0C, /* LoRa Power of 12 dBm */
	POWER13DBM = 0x0D, /* LoRa Power of 13 dBm */
	POWER14DBM = 0x0E, /* LoRa Power of 14 dBm */
	POWER15DBM = 0x0F, /* LoRa Power of 15 dBm */
	POWER16DBM = 0x10, /* LoRa Power of 16 dBm */
	POWER17DBM = 0x11, /* LoRa Power of 17 dBm */
	POWER18DBM = 0x12, /* LoRa Power of 18 dBm */
	POWER19DBM = 0x13, /* LoRa Power of 19 dBm */
	POWER20DBM = 0x14, /* LoRa Power of 20 dBm */
} LoRaPower_Typedef;

/* Bandwidth Enum */
typedef enum LoRaBandwidth_Typedef
{
	BW125KHZ = 0x00, /* LoRa Bandwidth of 125KHZ */
	BW250KHZ = 0x01, /* LoRa Bandwidth of 250KHZ */
	BW500KHZ = 0x02, /* LoRa Bandwidth of 500KHZ */
} LoRaBandwidth_Typedef;

/* Spreading Factor Enum */
typedef enum LoRaSpreadingFactor_Typedef
{
	SFFSK = 0x05, /* LoRa Spreading Factor of 5 / FSK */
	SF7 = 0x07,   /* LoRa Spreading Factor of 7 */
	SF8 = 0x08,   /* LoRa Spreading Factor of 8 */
	SF9 = 0x09,   /* LoRa Spreading Factor of 9 */
	SF10 = 0x0A,  /* LoRa Spreading Factor of 9 */
	SF11 = 0x0B,  /* LoRa Spreading Factor of 9 */
	SF12 = 0x0C,  /* LoRa Spreading Factor of 9 */
} LoRaSpreadingFactor_Typedef;

/* Coding Rate Enum */
typedef enum LoRaCodingRate_Typedef
{
	CR45 = 0x01, /* LoRa Coding Rate of 4/5 */
	CR46 = 0x02, /* LoRa Coding Rate of 4/6 */
	CR47 = 0x03, /* LoRa Coding Rate of 4/7 */
	CR48 = 0x04, /* LoRa Coding Rate of 4/8 */
} LoRaCodingRate_Typedef;

/* LoRaClass Enum */
typedef enum LoRaClass_Typedef
{
	LORACLASSA = 0x00,  /* LoRa Class A */
	LORACLASSB = 0x01,  /* LoRa Class B (RSV) */
	LORACLASSC = 0x02,  /* LoRa Class C */
} LoRaClass_Typedef;

/* LoRaClass Enum */
typedef enum LoRaClassWindow_Typedef
{
	WINDOW5S  = 0x00,  /* LoRa Class Window 5 seconds */
	WINDOW10S = 0x01,  /* LoRa Class Window 10 seconds */
	WINDOW15S = 0x02,  /* LoRa Class Window 15 seconds */
} LoRaClassWindow_Typedef;

/* Test commands */
typedef enum Test_Typedef
{
	CMD_SSOFF = 0x02,	  /* Turns off SS (Spread Spectrum) */
	CMD_SSON = 0x03,	   /* Turns on SS (Spread Spectrum) */
	CMD_RESET = 0x04,	  /* Resets the device */
	CMD_LOSESYNC = 0x05,   /* Loses sync */
	CMD_P2PON = 0x06,	  /* Turns on Peer to Peer */
	CMD_P2POFF = 0x07,	 /* Turns off Peer to Peer */
	CMD_TLIREPON = 0x0A,   /* Turns on TLI repeat */
	CMD_TLIREPOFF = 0x0B,  /* Turns off TLI repeat */
	CMD_50CHANNELS = 0x0C, /* 50 channels */
	CMD_35CHANNELS = 0x0D, /* 35 channels */
} Test_Typedef;

/* GPIO Enum */
typedef enum GPIO_Typedef
{
	GPIO0,
	GPIO1,
	GPIO2,
	GPIO3,
	GPIO4,
	GPIO5,
	GPIO6,
	GPIO7
} GPIO_Typedef;

/* GPIO mode enum */
typedef enum Mode_Typedef
{
	DIGITAL_IN,
	DIGITAL_OUT,
	ANALOG_IN = 3
} Mode_Typedef;

/* Pull resistor enum */
typedef enum Pull_Typedef
{
	PULL_OFF,
	PULLUP,
	PULLDOWN
} Pull_Typedef;

typedef enum MeshStatus_Typedef
{
	MESH_OK  = 0,
	MESH_ERROR = 1
} MeshStatus_Typedef;

/* Frame type */
typedef struct Frame_Typedef
{
	uint8_t buffer[MAX_BUFFER_SIZE];
	uint8_t size;
	bool command;
} Frame_Typedef;

typedef struct LoRaMESH_ModulationInfo
{
	uint8_t power;
	uint8_t bandwidth;
	uint8_t spreadingFactor;
	uint8_t codingRate;
} LoRaMESH_ModulationInfo;

typedef struct LoRaMESH_ClassInfo
{
	uint8_t loraClass;
	uint8_t window;
} LoRaMESH_ClassInfo;

typedef struct LoRaMESH_RssiInfo
{
	uint16_t gateway;
	uint8_t rssiOutgoing; // RSSI ida
	uint8_t rssiIncoming; // RSSI volta
} LoRaMESH_RssiInfo;

typedef struct LoRaMESH_NoiseInfo
{
	uint8_t min;
	uint8_t mean;
	uint8_t max;
} LoRaMESH_NoiseInfo;

typedef struct LoRaMESH_DiagnosisInfo
{
	uint8_t tempMin;
	uint8_t tempCurrent;
	uint8_t tempMax;
	uint8_t voltageMin;
	uint8_t voltageCurrent;
	uint8_t voltageMax;
} LoRaMESH_DiagnosisInfo;

typedef struct LoRaMESH_Info
{
	uint16_t id;
	uint16_t net;
	uint32_t uniqueId;
	uint8_t versionHardware;
	uint8_t revisionFirmware;
	uint8_t spreadSpectrum;
	uint8_t versionFirmware;
	uint8_t memorySlot;
} LoRaMESH_Info;


class LoRaMESH
{
	private:
		/* ----- Private Variables ----- */


		/* ----- Private Functions ----- */
		void FlushCommandSerial(void);
		void FlushTransparentSerial(void);

		String ConvertLoRaMESHInfoToString(LoRaMESH_Info info, bool compact = false);
		String ConvertLoRaMESHModInfoToString(LoRaMESH_ModulationInfo info, bool compact = false);
		String ConvertLoRaMESHClassInfoToString(LoRaMESH_ClassInfo info, bool compact = false);

		/**
		 * @brief Private function - Gets the ID, NET and UNIQUE ID info from the local or remote device
		 * @param idIn[in]: Remote device's id
		 * @param idOut[out]: Local device's id
		 * @param net[out]: Configured NET on local device
		 * @param uniqueId[out]: Device Unique ID 
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef LocalRemoteRead(uint16_t idIn, uint16_t *idOut, LoRaMESH_Info *readDeviceInfo);

	public:
		/* ----- Public global variables ----- */
		static SomeSerial *hSerialCommand;
		static SomeSerial *hSerialTransparent;

		static LoRaMESH_Info deviceInfo;
		static LoRaMESH_ModulationInfo modInfo;
		static LoRaMESH_ClassInfo classInfo;
		static uint32_t password;

		static bool isMaster;
		static uint32_t defaultTimeout;

		static LoRaMESH_Info remoteDeviceInfo;
		static LoRaMESH_ModulationInfo remoteModInfo;
		static LoRaMESH_ClassInfo remoteClassInfo;
		static uint32_t remotePassword;

		// uint8_t receivedPacket[MAX_BUFFER_SIZE];

		static MeshStatus_Typedef latestCommandStatus;
		static Frame_Typedef frame;
		/* ----- Public Functions Prototype ----- */
		LoRaMESH();

		static bool IsUndefined(void);
		static bool IsMaster(void);
		/**
		 * @brief Initializes Commands interface and the SoftwareSerial object on given Rx/Tx pins and Baudrate
		 * @param rxPin: Serial Rx pin
		 * @param txPin: Serial Tx pin
		 * @param baudRate: Serial baudrate, in bps
		 * @retval pointer to the SoftwareSerial object
		 */
		SomeSerial *InitCommandSerial(uint8_t rxPin, uint8_t txPin, uint32_t baudRate);
		MeshStatus_Typedef InitCommandSerial(SomeSerial *commandSerial, uint32_t baudRate);

		/**
		 * @brief Initializes Transparent interface and the SoftwareSerial object on given Rx/Tx pins and Baudrate
		 * @param rxPin: Serial Rx pin
		 * @param txPin: Serial Tx pin
		 * @param baudRate: Serial baudrate, in bps
		 * @retval pointer to the SoftwareSerial object
		 */
		SomeSerial *InitTransparentSerial(uint8_t rxPin, uint8_t txPin, uint32_t baudRate);
		MeshStatus_Typedef InitTransparentSerial(SomeSerial *transparentSerial, uint32_t baudRate);

		/**
		 * @brief Sets the internal Command Serial interface in the class
		 * @param commandSerial[in]: Pointer to the Serial Interface connected to the Command Serial Interface
		 */
		MeshStatus_Typedef SetCommandSerial(SomeSerial * commandSerial);
		
		/**
		 * @brief Sets the internal Transparent Serial interface in the class
		 * @param commandSerial[in]: Pointer to the Serial Interface connected to the Transparent Serial Interface
		 */
		MeshStatus_Typedef SetTransparentSerial(SomeSerial * transparentSerial);

		/**
		 * @brief Prepares a frame to transmission via commands interface
		 * @param id: Target device's ID
		 * @param command: Byte that indicates the command
		 * @param payload: pointer to the array holding the payload
		 * @param payloadSize: payload size
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef PrepareFrameCommand(uint16_t id, uint8_t command, uint8_t *payload, uint8_t payloadSize);

		/**
		 * @brief Prepares a frame to transmission via transparent interface
		 * @param id: Target device's ID
		 * @param payload: pointer to the array holding the payload
		 * @param payloadSize: payload size
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef PrepareFrameTransp(uint16_t id, uint8_t *payload, uint8_t payloadSize);

		/**
		 * @brief Sends a frame previously prepared by PrepareFrame
		 * @param None
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef SendPacket(void);

		/**
		 * @brief Prepares a frame to transmission directly
		 * @param hexCommand[i]: A String containing the command's hex bytes
		 * @param calculateCrc[in]: Include the CRC at the end of the frame
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef PrepareFrame(String hexCommand, bool calculateCrc);

		/**
		 * @brief Sends Hex bytes directly to the Serial Command interface
		 * @param command[in]: String with the HEX characters to send directly to command interface
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef SendDirectCommand(String hexCommand);

		/**
		 * @brief Sends Hex bytes directly to the Serial Command interface
		 * @param command[in]: String with the HEX characters to send directly to command interface
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef SendDirectTransparent(String hexCommand);

		/**
		 * @brief Receives a packet from the commands interface and stores it in the frame.buffer
		 * @param timeout[in]: reception timeout, in ms
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef ReceivePacketCommand(uint32_t timeout);

		/**
		 * @brief Receives a packet from the commands interface
		 * @param id[out]: ID from the received message
		 * @param command[out]: received command
		 * @param payload[out]: buffer where the received payload will be copied to
		 * @param payloadSize[out]: received payload size
		 * @param timeout: reception timeout, in ms
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef ReceivePacketCommand(uint16_t *id, uint8_t *command, uint8_t *payload, uint8_t *payloadSize, uint32_t timeout);
		
		/**
		 * @brief Receives a packet from the transparent interface and stores it in the frame.buffer
		 * @param timeout[in]: reception timeout, in ms
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef ReceivePacketTransp(uint32_t timeout);

		/**
		 * @brief Receives a packet from the transparent interface
		 * @param id[out]: ID from the received message
		 * @param payload[out]: buffer where the received payload will be copied to
		 * @param payloadSize[out]: received payload size
		 * @param timeout: reception timeout, in ms
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef ReceivePacketTransp(uint16_t *id, uint8_t *payload, uint8_t *payloadSize, uint32_t timeout);

		/**
		 * @brief Configures a GPIO of a device
		 * @param id: Target device's ID
		 * @param pin: pin to be configured (GPIO0 ... GPIO7)
		 * @param mode: operation mode (DIGITAL_IN, DIGITAL_OUT, ANALOG_IN)
		 * @param pull: internal pull resistor mode (PULL_OFF, PULLUP, PULLDOWN)
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef GpioConfig(uint16_t id, GPIO_Typedef pin, Mode_Typedef mode, Pull_Typedef pull);

		/**
		 * @brief Sets the logic level of a given pin
		 * @param id: Target device's ID
		 * @param pin: pin to be configured (GPIO0 ... GPIO7)
		 * @param value: 1 or 0
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef GpioWrite(uint16_t id, GPIO_Typedef pin, uint8_t value);

		/**
		 * @brief Reads the logic level (digital) or the analog value of a pin
		 * @param id: Target device's ID
		 * @param pin: pin to be configured (GPIO0 ... GPIO7)
		 * @param value[out]: 1 or 0 (digital mode); 0 to 4095 (analog mode)
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef GpioRead(uint16_t id, GPIO_Typedef pin, uint16_t *value);

		/**
		 * @brief Gets the ID, NET and UNIQUE ID info from the local device
		 * @param id[out]: Local device's id
		 * @param localDeviceInfo[out]: Local device's info
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef LocalRead(uint16_t *id, LoRaMESH_Info *localDeviceInfo);

		/**
		 * @brief Gets the ID, NET and UNIQUE ID info from the local device
		 * @param id[in]: Remote device's id
		 * @param localDeviceInfo[out]: Remote device's info
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef RemoteRead(uint16_t id, LoRaMESH_Info *remoteDeviceInfo);

		/**
		 * @brief Write the ID and NET to local or remote device
		 * @param uniqueId[in]: Target device's Unique ID
		 * @param newId[in]: ID to be configured in device
		 * @param newNet[in]: NET to be configured in device 
		 * @param newDeviceInfo[out]: Target device's new info
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef WriteConfig(uint32_t uniqueId, uint16_t newId, uint16_t newNet, LoRaMESH_Info *newDeviceInfo);

		/**
		 * @brief Reads the modulation params from a local or remote device
		 * @param id[in]: Target device's ID
		 * @param readModInfo[out]: LoRa's modulation info with the following parameters:
		 * @param power[out]: LoRa's power in dBm
		 * @param bandwidth[out]: Bandwidth with the following values: 0 (125 kHz), 1 (250 kHz) or 2 (500 kHz)
		 * @param spreadingFactor[out]: Spreading Factor with values from 7 to 12 for LoRa mode, or 5 for FSK mode
		 * @param codingRate[out]: LoRa's Coding Rate with the following values: 1 (4/5), 2 (4/6), 3 (4/7) or 4 (4/8)
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef ReadModParam(uint16_t id, LoRaMESH_ModulationInfo *readModInfo);

		/**
		 * @brief Write the modulation params to a local or remote device
		 * @param id[in]: Target device's ID
		 * @param writeModInfo[in]: LoRa's modulation info with the following parameters:
		 * @param power[in]: LoRa's power in dBm
		 * @param bandwidth[in]: Bandwidth with the following values: 0 (125 kHz), 1 (250 kHz) or 2 (500 kHz)
		 * @param spreadingFactor[in]: Spreading Factor with values from 7 to 12 for LoRa mode, or 5 for FSK mode
		 * @param codingRate[in]: LoRa's Coding Rate with the following values: 1 (4/5), 2 (4/6), 3 (4/7) or 4 (4/8)
		 * @retval MESH_OK or MESH_ERROR
		 */
		MeshStatus_Typedef WriteModParam(uint16_t id, LoRaMESH_ModulationInfo writeModInfo);

		MeshStatus_Typedef WriteClass(uint16_t id, LoRaMESH_ClassInfo *writeClassInfo);

		MeshStatus_Typedef WritePassword(uint16_t id, uint32_t password);

		MeshStatus_Typedef ReadDiagnosis(uint16_t id, LoRaMESH_DiagnosisInfo *diagnosisInfo);

		MeshStatus_Typedef ReadNoise(uint16_t id, LoRaMESH_NoiseInfo *noiseInfo);

		MeshStatus_Typedef ReadRSSI(uint16_t id, LoRaMESH_RssiInfo *rssiInfo);

		MeshStatus_Typedef TraceRoute(uint16_t id);


		MeshStatus_Typedef ReadClass(uint16_t id, LoRaMESH_ClassInfo *readClassInfo);

		String GetLocalDeviceInfoString(bool compact = false);
		String GetLocalDeviceModInfoString(bool compact = false);
		String GetLocalDeviceClassInfoString(bool compact);
		String GetRemoteDeviceInfoString(bool compact = false);
		String GetRemoteDeviceModInfoString(bool compact = false);
		String GetRemoteDeviceClassInfoString(bool compact = false);

		void ListenCommand();
		void ListenTransparent();

		/**
		 * @brief  Computes CRC16.
		 * @param  data_in[in]: Pointer to the input buffer.
		 * @param  length[in]: Buffer size
		 * @retval CRC16
		 */
		uint16_t ComputeCRC(uint8_t *data_in, uint16_t length);

		uint16_t ComputeCRCBusModule(uint8_t *data_in, uint16_t length);

		MeshStatus_Typedef PrepareFrameTranspBusModule(uint16_t id, uint8_t *payload, uint8_t payloadSize);
};

#endif /* LORAMESH_H_ */