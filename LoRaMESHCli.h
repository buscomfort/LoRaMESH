#ifndef LORAMESHCLI_H_
#define LORAMESHCLI_H_

#include "LoRaMESH.h"

#define LINE_BUF_SIZE 128 //Maximum input string length
#define ARG_BUF_SIZE 32   //Maximum argument string length
#define MAX_NUM_ARGS 8	//Maximum number of arguments

static bool error_flag = false;

static char line[LINE_BUF_SIZE];
static char args[MAX_NUM_ARGS][ARG_BUF_SIZE];

void cli_init(HardwareSerial *loggerSerial, LoRaMESH *loraMesh);
void loramesh_cli();

void read_line();
void parse_line();
int execute();
void cli_header();
void print_reply();

//Function declarations
int cmd_help();
int cmd_write();
int cmd_read();
int cmd_interface();
int cmd_exit();
void help_help();
void help_write();
void help_read();
void help_interface();
void help_exit();

//List of functions pointers corresponding to each command
static int (*commands_func[])(){
	&cmd_help,
	&cmd_write,
	&cmd_read,
	&cmd_interface,
	&cmd_exit};

//List of command names
static const char *commands_str[] = {
	"help",
	"write",
	"read",
	"interface",
	"exit"};

typedef enum CliCmd_Typedef
{
	CLI_CMD_HELP,
	CLI_CMD_WRITE,
	CLI_CMD_READ,
	CLI_CMD_INTERFACE,
	CLI_CMD_EXIT,
} CliCmd_Typedef;

//List of GENERAL for write/read sub command names
static const char *selection_args[] = {
	"local",
	"remote"};

typedef enum CliSel_Typedef
{
	CLI_SEL_LOCAL,
	CLI_SEL_REMOTE,
} CliSel_Typedef;

//List of WRITE sub command names
static const char *write_args[] = {
	"config",
	"modulation",
	"loraclass",
	"password"};

typedef enum CliWrite_Typedef
{
	CLI_WRITE_CONFIG,
	CLI_WRITE_MODULATION,
	CLI_WRITE_LORACLASS,
	CLI_WRITE_PASSWORD,
} CliWrite_Typedef;

//List of READ sub command names
static const char *read_args[] = {
	"config",
	"modulation",
	"loraclass",
	"diagnosis",
	"noise",
	"rssi",
	"spectrum"};

typedef enum CliRead_Typedef
{
	CLI_READ_CONFIG,
	CLI_READ_MODULATION,
	CLI_READ_LORACLASS,
	CLI_READ_DIAGNOSIS,
	CLI_READ_NOISE,
	CLI_READ_RSSI,
	CLI_READ_SPECTRUM,
} CliRead_Typedef;

//List of INTERFACE sub command names
static const char *interfacesel_args[] = {
	"command",
	"transparent"};

typedef enum CliInterfaceSel_Typedef
{
	CLI_IFACE_COMMAND,
	CLI_IFACE_TRANSPARENT,
} CliInterfaceSel_Typedef;

//List of INTERFACE sub subcommand names
static const char *interface_args[] = {
	"send",
	"receive",
	"listen"};

typedef enum CliInterface_Typedef
{
	CLI_IFACE_SEND,
	CLI_IFACE_RECEIVE,
	CLI_IFACE_LISTEN,
} CliInterface_Typedef;

static int num_commands = sizeof(commands_str) / sizeof(char *);

#endif /* LORAMESHCLI_H_ */