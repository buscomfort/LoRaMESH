#ifndef LORAMESHMENU_H_
#define LORAMESHMENU_H_

#include "LoRaMESH.h"

void header();
void mainMenu(HardwareSerial *loggerSerial, LoRaMESH *loraMesh);
void configMenu();
void writeConfigMenu();
void loraMenu();
void writeLoRaParamMenu();
void localReadMenu();
void interfaceMenu();
void commandInterface();
void transparentInterface();

#endif /* LORAMESHMENU_H_ */