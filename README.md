# LoRaMESH

[RadioEnge LoRaMESH](https://www.radioenge.com.br/solucoes/iot/34-modulo-loramesh.html) Library adapted by ruanmed, based on the original [RadioEnge LoRaMESH library](https://github.com/Radioenge/LoRaMESH).

## Requirements

This Arduino library has the following dependencies:

* [Arduino](https://github.com/arduino/Arduino) >= 1.8.10
    * Uses the in HardwareSerial.h
    * Uses in SoftwareSerial.h if avaiable (does not depend on it)
* [SomeSerial](https://github.com/asukiaaa/SomeSerial) by Asuki Kono >= 1.1.1

## Compatibility

This Arduino library was tested with the following Arduino boards:

* Arduino Uno - ATmega328P
* Arduino Due - ATSAM3X8E

It should be compatible with any Arduino Board that either has an implementation for the SoftwareSerial library, or has atleast 2 built in Serial ports.
