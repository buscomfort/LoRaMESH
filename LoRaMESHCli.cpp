#include "LoRaMESHCli.h"

static HardwareSerial *logger;
static LoRaMESH *localLM;

void cli_init(HardwareSerial *loggerSerial, LoRaMESH *loraMesh)
{
	logger = loggerSerial;
	localLM = loraMesh;
	logger->println(F("Welcome to this simple Arduino command line interface (CLI)."));
	logger->println(F("Type \"help\" to see a list of commands."));
}

void loramesh_cli()
{
	cli_header();
	logger->print("> ");

	read_line();
	if (!error_flag)
	{
		parse_line();
	}
	if (!error_flag)
	{
		execute();
	}

	memset(line, 0, LINE_BUF_SIZE);
	memset(args, 0, sizeof(args[0][0]) * MAX_NUM_ARGS * ARG_BUF_SIZE);

	error_flag = false;
}

void read_line()
{
	String line_string;

	while (!logger->available())
		;

	if (logger->available())
	{
		line_string = logger->readStringUntil('\n');
		if (line_string.length() < LINE_BUF_SIZE)
		{
			line_string.toCharArray(line, LINE_BUF_SIZE);
			logger->println(line_string);
		}
		else
		{
			logger->println(F("Input string too long."));
			error_flag = true;
		}
	}
}

void parse_line()
{
	char *argument;
	int counter = 0;

	argument = strtok(line, " ");

	while ((argument != NULL))
	{
		if (counter < MAX_NUM_ARGS)
		{
			if (strlen(argument) < ARG_BUF_SIZE)
			{
				strcpy(args[counter], argument);
				argument = strtok(NULL, " ");
				counter++;
			}
			else
			{
				logger->println(F("Input string too long."));
				error_flag = true;
				break;
			}
		}
		else
		{
			break;
		}
	}
}

int execute()
{
	for (int i = 0; i < num_commands; i++)
	{
		if (strcmp(args[0], commands_str[i]) == 0)
		{
			return (*commands_func[i])();
		}
	}

	logger->println(F("Invalid command. Type \"help\" for more."));
	return 0;
}

void cli_header()
{
	logger->println(F("\n_________________"));
	logger->println(F("->LOCAL DEVICE INFO"));
	logger->println(localLM->GetLocalDeviceInfoString(true));
	// logger->println(F("-->MODULATION INFO"));
	logger->println(localLM->GetLocalDeviceModInfoString(true));
	logger->println(localLM->GetLocalDeviceClassInfoString(true));
	logger->println(F("->REMOTE DEVICE INFO"));
	logger->println(localLM->GetRemoteDeviceInfoString(true));
	// logger->println(F("-->MODULATION INFO"));
	logger->println(localLM->GetRemoteDeviceModInfoString(true));
	logger->println(localLM->GetRemoteDeviceClassInfoString(true));
	logger->println(F("_________________"));
}

void print_reply()
{
	logger->print(F("FRAME SIZE: "));
	logger->println(localLM->frame.size);
	for (int c = 0; c < localLM->frame.size; c++)
	{
		logger->print(String(localLM->frame.buffer[c], HEX));
		logger->print(' ');
	}
}

int cmd_help()
{
	if (args[1] == NULL)
		help_help();
	else if (strcmp(args[1], commands_str[CLI_CMD_EXIT]) == 0)
		help_help();
	else if (strcmp(args[1], commands_str[CLI_CMD_WRITE]) == 0)
		help_write();
	else if (strcmp(args[1], commands_str[CLI_CMD_READ]) == 0)
		help_read();
	else if (strcmp(args[1], commands_str[CLI_CMD_INTERFACE]) == 0)
		help_interface();
	else if (strcmp(args[1], commands_str[CLI_CMD_EXIT]) == 0)
		help_exit();
	else
		help_help();

	return 0;
}

void help_help()
{
	logger->println(F("The following commands are available:"));

	for (int i = 0; i < num_commands; i++)
	{
		logger->print("  ");
		logger->println(commands_str[i]);
	}
	logger->println("");
	logger->println(F("You can for instance type \"help write\" for more info on the WRITE command."));
}

void help_write()
{
	logger->println(F("Write local or remote configurations to a LoRaMESH:"));
	logger->println(F("\t write { local | remote | UNIQUEID } config ID NET"));
	logger->println(F("\t\t where \"ID\" is the new id and"));
	logger->println(F("\t\t \"NET\" is the new net to be configured."));
	logger->println(F("\t write { local | remote | ID } modulation PW BW SF CR"));
	logger->println(F("\t write { local | remote | ID } loraclass CLASS WINDOW"));
	logger->println(F("\t write { local | remote | ID } password PASSWORD"));
}

void help_read()
{
	logger->println(F("Read local or remote configurations from a LoRaMESH:"));
	logger->println(F("\t read { local | remote | ID } config"));
	logger->println(F("\t read { local | remote | ID } modulation"));
	logger->println(F("\t read { local | remote | ID } loraclass"));
	logger->println(F("\t read { local | remote | ID } noise"));
	logger->println(F("\t read { local | remote | ID } rssi"));
	logger->println(F("\t read { local | remote | ID } spectrum"));
}

void help_interface()
{
	logger->println(F("Manipulate local LoRaMESH device interfaces':"));
	logger->println(F("\t interface { command | transparent } send BYTES"));
	logger->println(F("\t interface { command | transparent } { receive | listen }"));
}

void help_exit()
{
	logger->println(F("This will exit the CLI. To restart the CLI, restart the program."));
}

int cmd_write()
{
	uint16_t id = 0;
	uint32_t uniqueId = 0;
	if (strcmp(args[1], selection_args[CLI_SEL_LOCAL]) == 0)
	{
		id = localLM->deviceInfo.id;
		uniqueId = localLM->deviceInfo.uniqueId;
	}
	else if (strcmp(args[1], selection_args[CLI_SEL_REMOTE]) == 0)
	{
		id = localLM->remoteDeviceInfo.id;
		uniqueId = localLM->remoteDeviceInfo.uniqueId;
	}
	else
	{
		int selector = atoi(args[1]);
		if (selector >= 0)
		{
			id = (uint16_t)selector;
			uniqueId = (uint32_t)selector;
		}
		else
		{
			logger->println(F("Invalid {local | remote | {UNIQUEID | ID}}."));
			return 1;
		}
	}

	if (strcmp(args[2], write_args[CLI_WRITE_CONFIG]) == 0)
	{
		// write local config ID NET
		// write remote config ID NET
		// write UNIQUEID config ID NET
		int i = atoi(args[3]);
		int net = atoi(args[4]);
		uint16_t newId = (uint16_t)i;
		uint16_t newNet = (uint16_t)net;

		if (id >= 0) /* Verificação do ID */
		{
			if (net >= 0)
			{
				logger->println(F("SENDING COMMAND\nSEND STATUS: "));
				logger->println(localLM->WriteConfig(uniqueId, newId, newNet, NULL));
				print_reply();
			}
			else
			{
				logger->println(F("Invalid NET."));
			}
		}
		else
		{
			logger->println(F("Invalid ID."));
		}
	}
	else if (strcmp(args[2], write_args[CLI_WRITE_MODULATION]) == 0)
	{
		/* write local modulation PW BW SF CR */
		/* write remote modulation PW BW SF CR */
		/* write ID modulation PW BW SF CR */
		int pw = atoi(args[3]);
		int bw = atoi(args[4]);
		int sf = atoi(args[5]);
		int cr = atoi(args[6]);

		LoRaMESH_ModulationInfo modInfo;
		modInfo.power = (uint8_t)pw;
		modInfo.bandwidth = (uint8_t)bw;
		modInfo.spreadingFactor = (uint8_t)sf;
		modInfo.codingRate = (uint8_t)cr;
		if (pw >= 0)
		{
			if (bw >= 0)
			{
				if (sf >= 0)
				{
					if (cr >= 0)
					{
						logger->println(F("SENDING COMMAND\nSEND STATUS: "));
						logger->println(localLM->WriteModParam(id, modInfo));
						print_reply();
					}
					else
					{
						logger->println(F("Invalid CODING RATE."));
					}
				}
				else
				{
					logger->println(F("Invalid SPREADING FACTOR."));
				}
			}
			else
			{
				logger->println(F("Invalid BANDWIDTH."));
			}
		}
		else
		{
			logger->println(F("Invalid POWER."));
		}
	}
	else if (strcmp(args[2], write_args[CLI_WRITE_LORACLASS]) == 0)
	{
		// write local loraclass CLASS WINDOW
		// write remote loraclass CLASS WINDOW
		// write ID loraclass CLASS WINDOW
		int cl = atoi(args[3]);
		int win = atoi(args[4]);
		LoRaMESH_ClassInfo classInfo;
		classInfo.loraClass = cl;
		classInfo.window = win;
		if (cl >= 0)
		{
			if (win >= 0)
			{
				logger->println(F("SENDING COMMAND\nSEND STATUS: "));
				logger->println(localLM->WriteClass(id, &classInfo));
				print_reply();
			}
			else
			{
				logger->println(F("Invalid WINDOW."));
			}
		}
		else
		{
			logger->println(F("Invalid CLASS."));
		}
	}
	else if (strcmp(args[2], write_args[CLI_WRITE_PASSWORD]) == 0)
	{
		// write local password PASSWORD
		// write remote password PASSWORD
		// write ID password PASSWORD
		int pass = atoi(args[3]);
		uint32_t password = (uint32_t)pass;
		if (pass >= 0)
		{
			logger->println(F("SENDING COMMAND\nSEND STATUS: "));
			logger->println(localLM->WritePassword(id, password));
			print_reply();
		}
		else
		{
			logger->println(F("Invalid PASSWORD."));
		}
	}
	else
	{
		logger->println(F("Invalid command. Type \"help write\" to see how to use the WRITE command."));
	}

	return 0;
}

int cmd_read()
{
	uint16_t id = 0;
	if (strcmp(args[1], selection_args[CLI_SEL_LOCAL]) == 0)
	{
		id = localLM->deviceInfo.id;
	}
	else if (strcmp(args[1], selection_args[CLI_SEL_REMOTE]) == 0)
	{
		id = localLM->remoteDeviceInfo.id;
	}
	else
	{
		int selector = atoi(args[1]);
		if (selector >= 0)
		{
			id = (uint16_t)selector;
		}
		else
		{
			logger->println(F("Invalid {local | remote | {UNIQUEID | ID}}."));
			return 1;
		}
	}

	if (strcmp(args[2], read_args[CLI_READ_CONFIG]) == 0)
	{
		// read local config
		// read remote config
		// read ID config
		logger->println(F("SENDING COMMAND\nSEND STATUS: "));
		if (strcmp(args[1], selection_args[CLI_SEL_LOCAL]) == 0)
		{
			logger->println(localLM->LocalRead(&id, NULL));
		}
		else
		{
			logger->println(localLM->RemoteRead(id, NULL));
		}
		print_reply();
	}
	else if (strcmp(args[2], read_args[CLI_READ_MODULATION]) == 0)
	{
		// read local modulation
		// read remote modulation
		// read ID modulation
		logger->println(F("SENDING COMMAND\nSEND STATUS: "));
		logger->println(localLM->ReadModParam(id, NULL));
		print_reply();
	}
	else if (strcmp(args[2], read_args[CLI_READ_LORACLASS]) == 0)
	{
		// read local loraclass
		// read remote loraclass
		// read ID loraclass
		logger->println(F("SENDING COMMAND\nSEND STATUS: "));
		logger->println(localLM->ReadClass(id, NULL));
		print_reply();
	}
	else if (strcmp(args[2], read_args[CLI_READ_DIAGNOSIS]) == 0)
	{
		// read local diagnosis
		// read remote diagnosis
		// read ID diagnosis
		int i = atoi(args[3]);
		uint16_t id = (uint16_t)i;
		logger->println(F("SENDING COMMAND\nSEND STATUS: "));
		// logger->println(localLM->WritePassword(id, password));
		print_reply();
	}
	else if (strcmp(args[2], read_args[CLI_READ_NOISE]) == 0)
	{
		// read local noise
		// read remote noise
		// read ID noise
		logger->println(F("SENDING COMMAND\nSEND STATUS: "));
		// logger->println(localLM->WritePassword(id, password));
		print_reply();
	}
	else if (strcmp(args[2], read_args[CLI_READ_RSSI]) == 0)
	{
		// read local rssi
		// read remote rssi
		// read ID rssi
		// read { local | remote | ID } rssi loop XTIMES TIMEOUT
		LoRaMESH_RssiInfo rssi;			
		char c = 1;
		logger->print(F("READING RSSI (send 0 to quit):"));
		while (c != '0')
		{
			logger->println(F("SENDING COMMAND\nSEND STATUS: "));
			logger->println(localLM->ReadRSSI(id, &rssi));
			logger->print(F("GATEWAY: "));
			logger->println(String(rssi.gateway));
			logger->print(F("RSSI Incoming: "));
			logger->println(String(rssi.rssiIncoming));
			logger->print(F("RSSI Outgoing: "));
			logger->println(String(rssi.rssiOutgoing));
			print_reply();
			delay(1000);
			if (logger->available())
				c = logger->read();
		}
	}
	else if (strcmp(args[2], read_args[CLI_READ_SPECTRUM]) == 0)
	{
		// read local spectrum
		// read remote spectrum
		// read ID spectrum
		logger->println(F("SENDING COMMAND\nSEND STATUS: "));
		// logger->println(localLM->WritePassword(id, password));
		print_reply();
	}
	else
	{
		logger->println(F("Invalid command. Type \"help read\" to see how to use the READ command."));
	}
	return 0;
}

int cmd_interface()
{
	if (strcmp(args[1], interfacesel_args[CLI_IFACE_COMMAND]) == 0)
	{
		// interface command send BYTES
		// interface command receive
		// interface command listen
		if (strcmp(args[2], interface_args[CLI_IFACE_SEND]) == 0)
		{
			String toSend = "";
			char msg = -1;
			do
			{
				while (!logger->available())
					;
				msg = (char)logger->read();
				toSend += msg;
			} while (msg != '\r' && msg != '\n');
			logger->println(F("HEX CODE to send: "));
			logger->println(F("Message read:"));
			logger->println(toSend);
			logger->println(F("SENDING COMMAND"));
			logger->print(F("SEND STATUS: "));
			logger->println(localLM->SendDirectCommand(toSend));
			print_reply();
			logger->print(F("RECEIVE STATUS: "));
			logger->println(localLM->ReceivePacketCommand(5000));
			print_reply();
		}
		else if (strcmp(args[2], interface_args[CLI_IFACE_RECEIVE]) == 0)
		{
			if (localLM->ReceivePacketCommand(5000) != MESH_ERROR)
			{
				logger->println(F("COMMAND INTERFACE PACKET RECEIVED"));
				print_reply();
			}
			else
			{
				logger->println(F("NO PACKET RECEIVED IN COMMAND INTERFACE"));
			}
		}
		else if (strcmp(args[2], interface_args[CLI_IFACE_LISTEN]) == 0)
		{
			char c = 1;
			uint16_t waitNextByte = 500;
			bool newline = false;
			localLM->ListenCommand();
			while (localLM->hSerialCommand->available() > 0)
				localLM->hSerialCommand->read();
			logger->print(F("LISTENING TO THE COMMAND SERIAL (send 0 to quit):"));
			while (c != '0')
			{
				waitNextByte = 500;
				newline = true;
				while (waitNextByte > 0)
				{
					if (localLM->hSerialCommand->available() > 0)
					{
						if (newline)
							logger->println(' ');
						logger->print(String(localLM->hSerialCommand->read(), HEX));
						logger->print(' ');
						newline = false;
						waitNextByte = 500;
					}
					waitNextByte--;
					delay(1);
				}
				if (newline)
					logger->print('.');
				else
					logger->println(' ');
				if (logger->available())
					c = logger->read();
			}
		}
		else
		{
			logger->println(F("Invalid interface option. Type \"help interface\" to see how to use the INTERFACE command."));
		}
	}
	else if (strcmp(args[1], interfacesel_args[CLI_IFACE_TRANSPARENT]) == 0)
	{
		// interface transparent send BYTES
		// interface transparent receive
		// interface transparent listen
		if (strcmp(args[2], interface_args[CLI_IFACE_SEND]) == 0)
		{
			String toSend = "";
			logger->println(F("HEX CODE to send: "));
			logger->println(F("Message read:"));
			logger->println(toSend);
			logger->println(F("SENDING COMMAND"));
			logger->print(F("SEND STATUS: "));
			logger->println(localLM->SendDirectTransparent(toSend));

			logger->print(F("RECEIVE STATUS: "));
			logger->println(localLM->ReceivePacketTransp(5000));
			print_reply();
		}
		else if (strcmp(args[2], interface_args[CLI_IFACE_RECEIVE]) == 0)
		{
			if (localLM->ReceivePacketTransp(5000) != MESH_ERROR)
			{
				logger->println(F("COMMAND INTERFACE PACKET RECEIVED"));
				print_reply();
			}
			else
			{
				logger->println(F("NO PACKET RECEIVED IN TRANSPARENT INTERFACE"));
			}
		}
		else if (strcmp(args[2], interface_args[CLI_IFACE_LISTEN]) == 0)
		{
			char c = 1;
			uint16_t waitNextByte = 500;
			bool newline = false;
			localLM->ListenTransparent();
			while (localLM->hSerialTransparent->available() > 0)
				localLM->hSerialTransparent->read();
			logger->print(F("LISTENING TO THE TRANSPARENT SERIAL (send 0 to quit):"));
			while (c != '0')
			{
				waitNextByte = 500;
				newline = true;
				while (waitNextByte > 0)
				{
					if (localLM->hSerialTransparent->available() > 0)
					{
						if (newline)
							logger->println(' ');
						logger->print(String(localLM->hSerialTransparent->read(), HEX));
						logger->print(' ');
						newline = false;
						waitNextByte = 500;
					}
					waitNextByte--;
					delay(1);
				}
				if (newline)
					logger->print('.');
				else
					logger->println(' ');
				if (logger->available())
					c = logger->read();
			}
		}
		else
		{
			logger->println(F("Invalid interface option. Type \"help interface\" to see how to use the INTERFACE command."));
		}
	}
	else
	{
		logger->println(F("Invalid command. Type \"help interface\" to see how to use the INTERFACE command."));
	}

	return 0;
}

int cmd_exit()
{
	logger->println(F("Exiting CLI."));
	while (1)
		;
	return 0;
}