/* ---------------------------------------------------
        Radioenge Equipamentos de Telecomunicações
   ---------------------------------------------------
    This library contains a set of functions to configure
    and operate the EndDevice LoRaMESH Radioenge
  
  Date: 13/12/18
*/
#include "LoRaMESH.h"

/* ----- Initialize Private ----- */
SomeSerial * LoRaMESH::hSerialCommand = NULL;
SomeSerial * LoRaMESH::hSerialTransparent = NULL;
LoRaMESH_Info LoRaMESH::deviceInfo;
LoRaMESH_ModulationInfo LoRaMESH::modInfo;
LoRaMESH_ClassInfo LoRaMESH::classInfo;
uint32_t LoRaMESH::password;
uint32_t LoRaMESH::defaultTimeout = 5000;	// default timeout is 5 seconds
LoRaMESH_Info LoRaMESH::remoteDeviceInfo;
LoRaMESH_ModulationInfo LoRaMESH::remoteModInfo;
LoRaMESH_ClassInfo LoRaMESH::remoteClassInfo;
uint32_t LoRaMESH::remotePassword;
Frame_Typedef LoRaMESH::frame;

/* ----- Private Functions ----- */

void LoRaMESH::FlushCommandSerial(void)
{
	while (hSerialCommand->available() > 0) 
		hSerialCommand->read();
}

void LoRaMESH::FlushTransparentSerial(void)
{
	while (hSerialTransparent->available() > 0)
		hSerialTransparent->read();
}

String LoRaMESH::ConvertLoRaMESHInfoToString(LoRaMESH_Info info, bool compact)
{
	String infoString = "|ID: " + String(info.id);
	if (compact) 
		infoString += "\t\t\t";
	else
		infoString += "\n|";
	infoString += "NET: " + String(info.net);	
	if (compact) 
		infoString += "\t";
	else
		infoString += "\n|";
	infoString += "UNIQUE ID: " + String(info.uniqueId, HEX);
	if (compact) 
		infoString += '\t';
	else
		infoString += "\n|";
	infoString += "HW VER: " + String(info.versionHardware);
	infoString += "\n|FW VER: " + String(info.versionFirmware);
	if (compact) 
		infoString += "\t\t";
	else
		infoString += "\n|";
	infoString += "FW REV: " + String(info.revisionFirmware);
	if (compact) 
		infoString += "\t";
	else
		infoString += "\n|";
	infoString += "SPREAD SPECTRUM: " + String(info.spreadSpectrum);
	if (info.spreadSpectrum == 0) 
		infoString += " (LoRa)";
	else 
		infoString += " (FSK)";	
	if (compact) 
		infoString += "\t";
	else
		infoString += "\n|";
	infoString += "MEMORY SLOT: " + String(info.memorySlot);
	return infoString;
}

String LoRaMESH::ConvertLoRaMESHModInfoToString(LoRaMESH_ModulationInfo info, bool compact)
{
	String infoString = "|POWER: " + String(info.power) + " (dBm)";
	if (compact) 
		infoString += "\t";
	else
		infoString += "\n|";
	infoString += "BANDWIDTH: " + String(info.bandwidth) + " (";
	switch (info.codingRate)
	{
		case 0:
			infoString += "125"; 
			break;
		case 1:
			infoString += "250"; 
			break;
		case 2:
			infoString += "500"; 
			break;
		default:
			infoString += "unknow"; 
			break;
	}
	infoString += " kHz)";
	if (compact) 
		infoString += "\t";
	else
		infoString += "\n|";
	infoString += "SPREADING FACTOR: " + String(info.spreadingFactor);
	if (info.spreadingFactor >= 7 && info.spreadingFactor <= 12) 
		infoString += " (LoRa)";
	else 
		infoString += " (FSK)"; 
	if (compact) 
		infoString += '\t';
	else
		infoString += "\n|";
	infoString += "CODING RATE: " + String(info.codingRate);
	switch (info.codingRate)
	{
		case 1:
			infoString += " (4/5)"; 
			break;
		case 2:
			infoString += " (4/6)"; 
			break;
		case 3:
			infoString += " (4/7)"; 
			break;
		case 4:
			infoString += " (4/8)"; 
			break;
		default:
			infoString += " (unknow)"; 
			break;
	}
	return infoString;
}

String LoRaMESH::ConvertLoRaMESHClassInfoToString(LoRaMESH_ClassInfo info, bool compact)
{
	String infoString = "|Class: " + String(info.loraClass)+ " (";
	switch (info.loraClass)
	{
		case 0:
			infoString += "A"; 
			break;
		case 1:
			infoString += "B"; 
			break;
		case 2:
			infoString += "C"; 
			break;
		default:
			infoString += "unknow"; 
			break;
	}
	infoString += ')';
	if (compact) 
		infoString += '\t';
	else
		infoString += "\n|";
	infoString += "Class Window: " + String(info.window)+ " (";	
	switch (info.window)
	{
		case 0:
			infoString += "5s"; 
			break;
		case 1:
			infoString += "10s"; 
			break;
		case 2:
			infoString += "15s"; 
			break;
		default:
			infoString += "unknow"; 
			break;
	}
	infoString += ')';
	return infoString;
}

MeshStatus_Typedef LoRaMESH::LocalRemoteRead(uint16_t idIn, uint16_t *idOut, LoRaMESH_Info *readDeviceInfo)
{
	uint8_t bufferPayload[31];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;
	uint16_t id = 0;

	/* Asserts parameters */
	if (readDeviceInfo == NULL)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;
	/* Loads dummy bytes */
	for (i = 0; i < 3; i++)
		bufferPayload[i] = 0x00;

	/* Prepares frame for transmission */
	if ((idOut == NULL) && (idIn < 1023)) /* Remote Read */
		PrepareFrameCommand(idIn, CMD_REMOTEREAD, &bufferPayload[0], i);
	else if (idOut != NULL) /* Local read */
		PrepareFrameCommand(0, CMD_LOCALREAD, &bufferPayload[0], i);
	else
		return MESH_ERROR;

	/* Sends packet */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if ((command != CMD_REMOTEREAD) && (command != CMD_LOCALREAD))
		return MESH_ERROR;

	/* Stores the received data */
	readDeviceInfo->id = id;
	readDeviceInfo->net = (uint16_t)bufferPayload[0] | ((uint16_t)(bufferPayload[1]) << 8);;
	readDeviceInfo->uniqueId = (uint32_t)bufferPayload[2] |
				((uint32_t)(bufferPayload[3]) << 8) |
				((uint32_t)(bufferPayload[4]) << 16) |
				((uint32_t)(bufferPayload[5]) << 24);
	readDeviceInfo->versionHardware = bufferPayload[6];
	readDeviceInfo->revisionFirmware = bufferPayload[8];
	readDeviceInfo->spreadSpectrum = bufferPayload[12];
	readDeviceInfo->versionFirmware = bufferPayload[14];
	readDeviceInfo->memorySlot = bufferPayload[15];

	if (idOut != NULL) /* Local read */
	{
		*idOut = id;
		deviceInfo = *readDeviceInfo;
	} 
	else 
	{
		remoteDeviceInfo = *readDeviceInfo;
	}

	return MESH_OK;
}

/* ----- Public Function Definitions ----- */

LoRaMESH::LoRaMESH() 
{
	LoRaMESH::deviceInfo.id = -1;
	LoRaMESH::deviceInfo.net = -1;
	LoRaMESH::deviceInfo.uniqueId = -1;
	LoRaMESH::deviceInfo.versionHardware = -1;
	LoRaMESH::deviceInfo.revisionFirmware = -1;
	LoRaMESH::deviceInfo.spreadSpectrum = -1;
	LoRaMESH::deviceInfo.versionFirmware = -1;
	LoRaMESH::deviceInfo.memorySlot = -1;

	LoRaMESH::modInfo.power = -1; 				/* Local device power */
	LoRaMESH::modInfo.bandwidth = -1; 			/* Local device bandwidth*/
	LoRaMESH::modInfo.spreadingFactor = -1; 	/* Local device spreading factor*/
	LoRaMESH::modInfo.codingRate = -1; 		/* Local device coding rate */
	LoRaMESH::classInfo.loraClass = -1;
	LoRaMESH::classInfo.window = -1;
	LoRaMESH::password = -1;

	LoRaMESH::remoteDeviceInfo.id = -1;
	LoRaMESH::remoteDeviceInfo.net = -1;
	LoRaMESH::remoteDeviceInfo.uniqueId = -1;
	LoRaMESH::remoteDeviceInfo.versionHardware = -1;
	LoRaMESH::remoteDeviceInfo.revisionFirmware = -1;
	LoRaMESH::remoteDeviceInfo.spreadSpectrum = -1;
	LoRaMESH::remoteDeviceInfo.versionFirmware = -1;
	LoRaMESH::remoteDeviceInfo.memorySlot = -1;

	LoRaMESH::remoteModInfo.power = -1; 				/* Local device power */
	LoRaMESH::remoteModInfo.bandwidth = -1; 			/* Local device bandwidth*/
	LoRaMESH::remoteModInfo.spreadingFactor = -1; 	/* Local device spreading factor*/
	LoRaMESH::remoteModInfo.codingRate = -1; 		/* Local device coding rate */
	LoRaMESH::remoteClassInfo.loraClass = -1;
	LoRaMESH::remoteClassInfo.window = -1;
	LoRaMESH::remotePassword = -1;
}

bool LoRaMESH::IsUndefined(void)
{
	if (deviceInfo.id == -1)
		return true;
	else
		return false;
}

bool LoRaMESH::IsMaster(void)
{
	if (deviceInfo.id == 0x00)
		return true;
	else
		return false;
}

SomeSerial* LoRaMESH::InitCommandSerial(uint8_t rxPin, uint8_t txPin, uint32_t baudRate)
{
	/* If SoftwareSerial is not supported, uses HardwareSerial 2 */
	#ifdef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	static SomeSerial radioCommandSerial(&Serial2);
	#else
	static SomeSerial radioCommandSerial(rxPin, txPin);
	#endif
	/* filter not used baudrates */
	radioCommandSerial.begin(baudRate);

	/* Run local read */
	LocalRead(&deviceInfo.id, &deviceInfo);

	hSerialCommand = &radioCommandSerial;
	return &radioCommandSerial;
}

MeshStatus_Typedef LoRaMESH::InitCommandSerial(SomeSerial *commandSerial, uint32_t baudRate)
{
	if (commandSerial == NULL)
		return MESH_ERROR;
	/* filter not used baudrates */
	commandSerial->begin(baudRate);

	/* Run local read */
	LocalRead(&deviceInfo.id, &deviceInfo);

	hSerialCommand = commandSerial;
	return MESH_OK;
}

SomeSerial* LoRaMESH::InitTransparentSerial(uint8_t rxPin, uint8_t txPin, uint32_t baudRate)
{
	/* If SoftwareSerial is not supported, uses HardwareSerial 3 */
	#ifdef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	// static SomeSerial radioTransparentSerial(&Serial3);
	static SomeSerial radioTransparentSerial(&Serial2);
	#else
	static SomeSerial radioTransparentSerial(rxPin, txPin);
	#endif
	/* filter not used baudrates */
	radioTransparentSerial.begin(baudRate);

	hSerialTransparent = &radioTransparentSerial;
	return &radioTransparentSerial;
}

MeshStatus_Typedef LoRaMESH::InitTransparentSerial(SomeSerial *transparentSerial, uint32_t baudRate)
{
	if (transparentSerial == NULL)
		return MESH_ERROR;
	transparentSerial->begin(baudRate);

	hSerialTransparent = transparentSerial;
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::SetCommandSerial(SomeSerial * commandSerial)
{
	if (commandSerial == NULL)
		return MESH_ERROR;
	hSerialCommand = commandSerial;
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::SetTransparentSerial(SomeSerial * transparentSerial)
{
	if (transparentSerial == NULL)
		return MESH_ERROR;
	hSerialTransparent = transparentSerial;
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::PrepareFrameCommand(uint16_t id, uint8_t command, uint8_t *payload, uint8_t payloadSize)
{
	if (payload == NULL)
		return MESH_ERROR;
	if (id > 1023)
		return MESH_ERROR;

	uint16_t crc = 0;

	frame.size = payloadSize + 5;

	/* Loads the target's ID */
	frame.buffer[0] = id & 0xFF;
	frame.buffer[1] = (id >> 8) & 0x03;

	/* Loads the command */
	frame.buffer[2] = command;

	if ((payloadSize >= 0) && (payloadSize < MAX_PAYLOAD_SIZE))
	{
		/* Loads the payload */
		memcpy(&(frame.buffer[3]), payload, payloadSize);

		/* Computes CRC */
		crc = ComputeCRC((&frame.buffer[0]), payloadSize + 3);
		frame.buffer[payloadSize + 3] = crc & 0xFF;
		frame.buffer[payloadSize + 4] = (crc >> 8) & 0xFF;
	}
	else
	{
		/* Invalid payload size */
		memset(&frame.buffer[0], 0, MAX_BUFFER_SIZE);
		return MESH_ERROR;
	}

	frame.command = true;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::PrepareFrameTransp(uint16_t id, uint8_t *payload, uint8_t payloadSize)
{
	uint8_t i = 0;

	if (payload == NULL)
		return MESH_ERROR;
	if (id > 1023)
		return MESH_ERROR;
	if (IsUndefined())
		return MESH_ERROR;

	if ((id != 0) && IsMaster()) /* Is master */
	{
		frame.size = payloadSize + 2;
		/* Loads the target's ID */
		frame.buffer[i++] = id & 0xFF;
		frame.buffer[i++] = (id >> 8) & 0x03;
	}
	else
	{
		frame.size = payloadSize;
	}

	if ((payloadSize >= 0) && (payloadSize < MAX_PAYLOAD_SIZE))
	{
		/* Loads the payload */
		memcpy(&frame.buffer[i], payload, payloadSize);
	}
	else
	{
		/* Invalid payload size */
		memset(&frame.buffer[0], 0, MAX_BUFFER_SIZE);
		return MESH_ERROR;
	}

	frame.command = false;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::SendPacket(void)
{
	if (frame.size == 0)
		return MESH_ERROR;
	if ((hSerialCommand == NULL) && (frame.command))
		return MESH_ERROR;
	if ((hSerialTransparent == NULL) && !(frame.command))
		return MESH_ERROR;

	if (frame.command)
		hSerialCommand->write(frame.buffer, frame.size);
	else 
		hSerialTransparent->write(frame.buffer, frame.size);
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::PrepareFrame(String hexCommand, bool calculateCrc)
{
	if (hexCommand.length() > (MAX_PAYLOAD_SIZE - 2)*2)
		return MESH_ERROR;

	uint16_t crc = 0; 
	uint16_t payloadSize = hexCommand.length()/2;
  	char hex[3];
	
	frame.size = payloadSize + 2;
	for (int c = 0; c < hexCommand.length(); c+=2)
	{
		hex[0] = hexCommand[c];
		hex[1] = hexCommand[c + 1];
    	hex[2] = '\0';

		/* Loads the payload */
		frame.buffer[(unsigned int) c/2] = (uint8_t)strtol(hex, NULL, 16);
	}

	if ((hexCommand.length() >= 0) && (hexCommand.length() < MAX_PAYLOAD_SIZE))
	{
		/* Computes CRC */
		if (calculateCrc)
		{
			crc = ComputeCRC((&frame.buffer[0]), payloadSize);
			frame.buffer[payloadSize + 0] = crc & 0xFF;
			frame.buffer[payloadSize + 1] = (crc >> 8) & 0xFF;
		}
	}
	else
	{
		/* Invalid payload size */
		memset(&frame.buffer[0], 0, MAX_BUFFER_SIZE);
		return MESH_ERROR;
	}

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::SendDirectCommand(String hexCommand)
{
	if (PrepareFrame(hexCommand, true) != MESH_OK)
		return MESH_ERROR;
	frame.command = true;

	ListenCommand();
	FlushCommandSerial();

	if (SendPacket()!=MESH_OK) {
		return MESH_ERROR;
	}
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::SendDirectTransparent(String hexCommand)
{
	if (PrepareFrame(hexCommand, false) != MESH_OK)
		return MESH_ERROR;
	frame.command = false;

	ListenTransparent();
	FlushTransparentSerial();

	if (SendPacket()!=MESH_OK)
		return MESH_ERROR;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReceivePacketCommand(uint32_t timeout)
{
	uint16_t waitNextByte = 500;
	uint8_t i = 0;
	uint16_t crc = 0;

	/* Assert parameters */
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialCommand->isSoftwareSerial() && !hSerialCommand->thisSoftwareSerial->isListening())
		hSerialCommand->thisSoftwareSerial->listen();
	#endif

	/* Waits for reception */
	while (((timeout > 0) || (i > 0)) && (waitNextByte > 0))
	{
		if (hSerialCommand->available() > 0)
		{
			frame.buffer[i++] = hSerialCommand->read();
			waitNextByte = 500;
		}

		if (i > 0)
			waitNextByte--;
		timeout--;
		delay(1);
	}

	/* In case it didn't get any data */
	if ((timeout == 0) && (i == 0)) {
		return MESH_ERROR;
	}

	// /* Checks CRC16 */
	// crc = (uint16_t)frame.buffer[i - 2] | ((uint16_t)frame.buffer[i - 1] << 8);
	// if (ComputeCRC(&frame.buffer[0], i - 2) != crc)
	// 	return MESH_ERROR;

	frame.size = i;
	// /* Copies ID */
	// *id = (uint16_t)frame.buffer[0] | ((uint16_t)frame.buffer[1] << 8);
	// /* Copies command */
	// *command = frame.buffer[2];
	// /* Copies payload size */
	// *payloadSize = i - 5;
	// /* Copies payload */
	// memcpy(payload, &frame.buffer[3], i - 5);

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReceivePacketCommand(uint16_t *id, uint8_t *command, uint8_t *payload, uint8_t *payloadSize, uint32_t timeout)
{
	uint16_t waitNextByte = 500;
	uint8_t i = 0;
	uint16_t crc = 0;

	/* Assert parameters */
	if (id == NULL)
		return MESH_ERROR;
	if (command == NULL)
		return MESH_ERROR;
	if (payload == NULL)
		return MESH_ERROR;
	if (payloadSize == NULL)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialCommand->isSoftwareSerial() && !hSerialCommand->thisSoftwareSerial->isListening())
		hSerialCommand->thisSoftwareSerial->listen();
	#endif

	/* Waits for reception */
	while (((timeout > 0) || (i > 0)) && (waitNextByte > 0))
	{
		if (hSerialCommand->available() > 0)
		{
			frame.buffer[i++] = hSerialCommand->read();
			waitNextByte = 500;
		}

		if (i > 0)
			waitNextByte--;
		timeout--;
		delay(1);
	}

	/* In case it didn't get any data */
	if ((timeout == 0) && (i == 0))
		return MESH_ERROR;

	/* Checks CRC16 */
	crc = (uint16_t)frame.buffer[i - 2] | ((uint16_t)frame.buffer[i - 1] << 8);
	if (ComputeCRC(&frame.buffer[0], i - 2) != crc)
		return MESH_ERROR;

	/* Copies ID */
	*id = (uint16_t)frame.buffer[0] | ((uint16_t)frame.buffer[1] << 8);
	/* Copies command */
	*command = frame.buffer[2];
	/* Copies payload size */
	*payloadSize = i - 5;
	/* Copies payload */
	memcpy(payload, &frame.buffer[3], i - 5);

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReceivePacketTransp(uint32_t timeout)
{
	uint16_t waitNextByte = 500;
	uint8_t i = 0;

	/* Assert parameters */
	if (hSerialTransparent == NULL)
		return MESH_ERROR;
	if (IsUndefined())
		return MESH_ERROR;

	#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialTransparent->isSoftwareSerial() && !hSerialTransparent->thisSoftwareSerial->isListening())
		hSerialTransparent->thisSoftwareSerial->listen();
	#endif

	/* Waits for reception */
	while (((timeout > 0) || (i > 0)) && (waitNextByte > 0))
	{
		if (hSerialTransparent->available() > 0)
		{
			frame.buffer[i++] = hSerialTransparent->read();
			waitNextByte = 500;
		}

		if (i > 0)
			waitNextByte--;
		timeout--;
		delay(1);
	}

	/* In case it didn't get any data */
	if ((timeout == 0) && (i == 0))
		return MESH_ERROR;

	frame.size = i;

	// if (IsMaster())
	// {
	// 	/* Copies ID */
	// 	*id = (uint16_t)frame.buffer[0] | ((uint16_t)frame.buffer[1] << 8);
	// 	/* Copies payload size */
	// 	*payloadSize = i - 2;
	// 	/* Copies payload */
	// 	memcpy(payload, &frame.buffer[3], i - 2);
	// }
	// else
	// {
	// 	/* Copies payload size */
	// 	*payloadSize = i;
	// 	/* Copies payload */
	// 	memcpy(payload, &frame.buffer[0], i);
	// }

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReceivePacketTransp(uint16_t *id, uint8_t *payload, uint8_t *payloadSize, uint32_t timeout)
{
	uint16_t waitNextByte = 500;
	uint8_t i = 0;

	/* Assert parameters */
	if ((id == NULL) && IsMaster())
		return MESH_ERROR;
	if (payload == NULL)
		return MESH_ERROR;
	if (payloadSize == NULL)
		return MESH_ERROR;
	if (hSerialTransparent == NULL)
		return MESH_ERROR;
	if (IsUndefined())
		return MESH_ERROR;

	#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialTransparent->isSoftwareSerial() && !hSerialTransparent->thisSoftwareSerial->isListening())
		hSerialTransparent->thisSoftwareSerial->listen();
	#endif

	/* Waits for reception */
	while (((timeout > 0) || (i > 0)) && (waitNextByte > 0))
	{
		if (hSerialTransparent->available() > 0)
		{
			frame.buffer[i++] = hSerialTransparent->read();
			waitNextByte = 500;
		}

		if (i > 0)
			waitNextByte--;
		timeout--;
		delay(1);
	}

	/* In case it didn't get any data */
	if ((timeout == 0) && (i == 0))
		return MESH_ERROR;

	if (IsMaster())
	{
		/* Copies ID */
		*id = (uint16_t)frame.buffer[0] | ((uint16_t)frame.buffer[1] << 8);
		/* Copies payload size */
		*payloadSize = i - 2;
		/* Copies payload */
		memcpy(payload, &frame.buffer[3], i - 2);
	}
	else
	{
		/* Copies payload size */
		*payloadSize = i;
		/* Copies payload */
		memcpy(payload, &frame.buffer[0], i);
	}

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::GpioConfig(uint16_t id, GPIO_Typedef pin, Mode_Typedef mode, Pull_Typedef pull)
{
	uint16_t crc = 0;
	uint8_t bufferPayload[10];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;

	/* Assert parameters */
	if (id > 1023)
		return MESH_ERROR;
	if (pin > GPIO7)
		return MESH_ERROR;
	if ((mode != DIGITAL_IN) && (mode != DIGITAL_OUT) && (mode != ANALOG_IN))
		return MESH_ERROR;
	if ((pull != PULL_OFF) && (pull != PULLUP) && (pull != PULLDOWN))
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads subcommand : Config */
	bufferPayload[i++] = 0x02;
	/* Loads pin */
	bufferPayload[i++] = pin;
	/* Loads pull */
	bufferPayload[i++] = pull;
	/* Loads mode */
	bufferPayload[i++] = mode;

	/* Prepares frame for transmission */
	PrepareFrameCommand(id, CMD_GPIOCONFIG, &bufferPayload[0], i);

	/* Sends frame */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_GPIOCONFIG)
		return MESH_ERROR;

	/* Checks error bit */
	if (bufferPayload[1] != 0)
		return MESH_ERROR;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::GpioWrite(uint16_t id, GPIO_Typedef pin, uint8_t value)
{
	uint16_t crc = 0;
	uint8_t bufferPayload[10];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;

	/* Assert parameters */
	if (id > 1023)
		return MESH_ERROR;
	if (pin > GPIO7)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads subcommand : Write */
	bufferPayload[i++] = 0x01;
	/* Loads pin */
	bufferPayload[i++] = pin;
	/* Loads value */
	bufferPayload[i++] = (value != 0) ? 1 : 0;
	bufferPayload[i++] = 0;

	/* Prepares frame for transmission */
	PrepareFrameCommand(id, CMD_GPIOCONFIG, &bufferPayload[0], i);

	/* Sends frame */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_GPIOCONFIG)
		return MESH_ERROR;

	/* Checks error bit */
	if (bufferPayload[1] != 0)
		return MESH_ERROR;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::GpioRead(uint16_t id, GPIO_Typedef pin, uint16_t *value)
{
	uint8_t crc = 0;
	uint8_t bufferPayload[10];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;

	/* Asserts parameters */
	if (id > 1023)
		return MESH_ERROR;
	if (pin > GPIO7)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;
	if (value == NULL)
		return MESH_ERROR;

	/* Loads subcommand : Read */
	bufferPayload[i++] = 0x00;
	/* Loads pin */
	bufferPayload[i++] = pin;

	bufferPayload[i++] = 0;
	bufferPayload[i++] = 0;

	/* Prepares frame for transmission */
	PrepareFrameCommand(id, CMD_GPIOCONFIG, &bufferPayload[0], i);

	/* Sends packet */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_GPIOCONFIG)
		return MESH_ERROR;

	/* Checks the error bit */
	if (bufferPayload[1] != 0)
		return MESH_ERROR;

	/* Returns the read value */
	*value = (uint16_t)bufferPayload[4] | ((uint16_t)(bufferPayload[3] & 0x0F) << 8);

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::LocalRead(uint16_t *id, LoRaMESH_Info *localDeviceInfo)
{	
	LoRaMESH_Info tempInfo;
	if (localDeviceInfo == NULL)
		localDeviceInfo = &tempInfo;
	return LocalRemoteRead(0xFFFF, id, localDeviceInfo);
}

MeshStatus_Typedef LoRaMESH::RemoteRead(uint16_t id, LoRaMESH_Info *remoteDeviceInfo)
{
	LoRaMESH_Info tempInfo;
	if (remoteDeviceInfo == NULL)
		remoteDeviceInfo = &tempInfo;
	return LocalRemoteRead(id, NULL, remoteDeviceInfo);
}

MeshStatus_Typedef LoRaMESH::WriteConfig(uint32_t uniqueId, uint16_t newId, uint16_t newNet, LoRaMESH_Info *newDeviceInfo)
{
	uint8_t bufferPayload[32];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;
	LoRaMESH_Info tempInfo;
	if (newDeviceInfo == NULL)
		newDeviceInfo = &tempInfo;
	/* Assert parameters */
	if (newId > 1023)
		return MESH_ERROR;
	if (newNet > 1023)
		return MESH_ERROR;
	if (newDeviceInfo == NULL)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads new NET value in bytes 3 and 4 */
	bufferPayload[i++] = (uint8_t)((newNet & 0x00FF) >> 0);
	bufferPayload[i++] = (uint8_t)((newNet & 0xFF00) >> 8);
	/* Loads the UID in bytes 5, 6, 7 and 8 */
	bufferPayload[i++] = (uint8_t)((uniqueId & 0x000000FF) >> 8);
	bufferPayload[i++] = (uint8_t)((uniqueId & 0x0000FF00) >> 8);
	bufferPayload[i++] = (uint8_t)((uniqueId & 0x00FF0000) >> 16);
	bufferPayload[i++] = (uint8_t)((uniqueId & 0xFF000000) >> 24);
	/* Loads default frame value in bytes 9, 10, 11, 12 and 13 */
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x00;

	/* Prepares frame for transmission */
	PrepareFrameCommand(newId, CMD_WRITECONFIG, &bufferPayload[0], i);

	/* Sends frame */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&newId, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_WRITECONFIG)
		return MESH_ERROR;

	newDeviceInfo->id = newId;
	newDeviceInfo->net = (uint16_t)bufferPayload[0] | ((uint16_t)(bufferPayload[1]) << 8);;
	newDeviceInfo->uniqueId = (uint32_t)bufferPayload[2] |
				((uint32_t)(bufferPayload[3]) << 8) |
				((uint32_t)(bufferPayload[4]) << 16) |
				((uint32_t)(bufferPayload[5]) << 24);
	newDeviceInfo->versionHardware = bufferPayload[6];
	newDeviceInfo->revisionFirmware = bufferPayload[8];
	newDeviceInfo->spreadSpectrum = bufferPayload[12];
	newDeviceInfo->versionFirmware = bufferPayload[14];
	newDeviceInfo->memorySlot = bufferPayload[15];

	/* Local write */
	if (uniqueId == deviceInfo.uniqueId) 
		deviceInfo = *newDeviceInfo;
	else 
		remoteDeviceInfo = *newDeviceInfo;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReadModParam(uint16_t id, LoRaMESH_ModulationInfo *readModInfo)
{
	uint8_t bufferPayload[12];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;
	LoRaMESH_ModulationInfo tempInfo;
	if (readModInfo == NULL)
		readModInfo = &tempInfo;

	/* Assert parameters */
	if (id > 1023)
		return MESH_ERROR;
	if (readModInfo == NULL)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads modulation reading sequence in bytes 3, 4 and 5 */
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x01;
	bufferPayload[i++] = 0x00;

	/* Prepares frame for transmission */
	PrepareFrameCommand(id, CMD_LORAPARAMETER, &bufferPayload[0], i);

	/* Sends frame */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_LORAPARAMETER)
		return MESH_ERROR;
	/* Checks byte 3 from command, must be 0x00 */
	if (bufferPayload[0] != 0x00)
		return MESH_ERROR;
	readModInfo->power = bufferPayload[1];
	readModInfo->bandwidth= bufferPayload[2];
	readModInfo->spreadingFactor = bufferPayload[3];
	readModInfo->codingRate = bufferPayload[4];
	if (id == deviceInfo.id) /* Local device modulation param change */
		modInfo = *readModInfo;
	else 
		remoteModInfo = *readModInfo;
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::WriteModParam(uint16_t id, LoRaMESH_ModulationInfo writeModInfo)
{
	uint8_t bufferPayload[12];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;

	/* Assert parameters */
	if (id > 1023)
		return MESH_ERROR;
	if (writeModInfo.power < 0 || writeModInfo.power > 20)
		return MESH_ERROR;
	if (writeModInfo.bandwidth < 0 || writeModInfo.bandwidth > 2)
		return MESH_ERROR;
	if (writeModInfo.spreadingFactor < 5 || writeModInfo.spreadingFactor > 12 || writeModInfo.spreadingFactor == 6)
		return MESH_ERROR;
	if (writeModInfo.codingRate < 1 || writeModInfo.codingRate > 4)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads modulation writing bit in byte 3 */
	bufferPayload[i++] = 0x01;
	/* Loads power in byte 4 */
	bufferPayload[i++] = writeModInfo.power;
	/* Loads Bandwidth in byte 5 */
	bufferPayload[i++] = writeModInfo.bandwidth;
	/* Loads Spreading Factor in byte 6 */
	bufferPayload[i++] = writeModInfo.spreadingFactor;
	/* Loads Coding Rate in byte 7 */
	bufferPayload[i++] = writeModInfo.codingRate;

	/* Prepares frame for transmission */
	PrepareFrameCommand(id, CMD_LORAPARAMETER, &bufferPayload[0], i);

	/* Sends frame */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;
	/* Checks command */
	if (command != CMD_LORAPARAMETER)
		return MESH_ERROR;
	/* Checks byte 3 from received command response, must be 0x00 */
	if (bufferPayload[0] != 0x01)
		return MESH_ERROR;

	writeModInfo.power = bufferPayload[1];
	writeModInfo.bandwidth = bufferPayload[2];
	writeModInfo.spreadingFactor = bufferPayload[3];
	writeModInfo.codingRate = bufferPayload[4];

	if (id == deviceInfo.id) /* Local device modulation param change */
	{
		modInfo = writeModInfo;
	}
	else 
	{
		remoteDeviceInfo.id = id;
		remoteModInfo = writeModInfo;
	}

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::WriteClass(uint16_t id, LoRaMESH_ClassInfo *writeClassInfo)
{
	uint8_t bufferPayload[31];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;
	/* Asserts parameters */
	if (writeClassInfo == NULL)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads dummy bytes */
	bufferPayload[i++] = 0x00;
	/* Specifies it's a Write Class Command */
	bufferPayload[i++] = writeClassInfo->loraClass;
	bufferPayload[i++] = writeClassInfo->window;
	/* Loads dummy bytes */
	bufferPayload[i++] = 0x00;

	/* Prepares frame for transmission */
	if (PrepareFrameCommand(id, CMD_LORACLASS, &bufferPayload[0], i) != MESH_OK)
		return MESH_ERROR;

	/* Sends packet */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_LORACLASS)
		return MESH_ERROR;

	writeClassInfo->loraClass = bufferPayload[1];
	writeClassInfo->window = bufferPayload[2];

	if (id == deviceInfo.id) /* Local read */
		classInfo = *writeClassInfo;
	else 
		remoteClassInfo = *writeClassInfo;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::WritePassword(uint16_t id, uint32_t password)
{
	uint8_t bufferPayload[31];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;
	/* Asserts parameters */
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads password writing bit in byte 3, subcommand 0x04 */
	bufferPayload[i++] = 0x04;
	/* Loads the password in bytes 4, 5, 6 and 7 */
	bufferPayload[i++] = (uint8_t)((password & 0x000000FF) >> 8);
	bufferPayload[i++] = (uint8_t)((password & 0x0000FF00) >> 8);
	bufferPayload[i++] = (uint8_t)((password & 0x00FF0000) >> 16);
	bufferPayload[i++] = (uint8_t)((password & 0xFF000000) >> 24);

	/* Prepares frame for transmission */
	if (PrepareFrameCommand(id, CMD_SPECIALCONFIG, &bufferPayload[0], i) != MESH_OK)
		return MESH_ERROR;

	/* Sends packet */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_SPECIALCONFIG)
		return MESH_ERROR;
	/* Checks byte 3 from received command response, must be 0x04 */
	if (bufferPayload[0] != 0x04)
		return MESH_ERROR;
	/* Checks byte 4 from received command response, must be 0x00 */
	if (bufferPayload[1] != 0x00)
		return MESH_ERROR;

	if (id == deviceInfo.id) /* Local password write */
		LoRaMESH::password = password;
	else 
		LoRaMESH::remotePassword = password;

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReadDiagnosis(uint16_t id, LoRaMESH_DiagnosisInfo *diagnosisInfo)
{
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReadNoise(uint16_t id, LoRaMESH_NoiseInfo *noiseInfo)
{
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReadRSSI(uint16_t id, LoRaMESH_RssiInfo *rssiInfo)
{
	uint8_t bufferPayload[10];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;
	LoRaMESH_RssiInfo tempInfo;
	if (rssiInfo == NULL)
		rssiInfo = &tempInfo;

	/* Assert parameters */
	if (id > 1023)
		return MESH_ERROR;
	if (rssiInfo == NULL)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads RSSI reading sequence in bytes 3, 4 and 5 */
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x00;

	/* Prepares frame for transmission */
	PrepareFrameCommand(id, CMD_READRSSI, &bufferPayload[0], i);

	/* Sends frame */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_READRSSI)
		return MESH_ERROR;
	rssiInfo->gateway = ((uint16_t)(bufferPayload[1]) << 8) | ((uint16_t)(bufferPayload[0]) << 0);
	rssiInfo->rssiIncoming = (uint8_t) bufferPayload[2];
	rssiInfo->rssiOutgoing = (uint8_t) bufferPayload[3];

	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::TraceRoute(uint16_t id)
{
	return MESH_OK;
}

MeshStatus_Typedef LoRaMESH::ReadClass(uint16_t id, LoRaMESH_ClassInfo *readClassInfo)
{
	uint8_t bufferPayload[31];
	uint8_t payloadSize;
	uint8_t i = 0;
	uint8_t command;
	LoRaMESH_ClassInfo tempInfo;
	if (readClassInfo == NULL)
		readClassInfo = &tempInfo;
	/* Asserts parameters */
	if (readClassInfo == NULL)
		return MESH_ERROR;
	if (hSerialCommand == NULL)
		return MESH_ERROR;

	/* Loads dummy bytes */
	bufferPayload[i++] = 0x00;
	/* Specifies it's a Read Class Command */
	bufferPayload[i++] = 0xFF;
	/* Loads dummy bytes */
	bufferPayload[i++] = 0x00;
	bufferPayload[i++] = 0x00;

	/* Prepares frame for transmission */
	if (PrepareFrameCommand(id, CMD_LORACLASS, &bufferPayload[0], i) != MESH_OK)
		return MESH_ERROR;

	/* Sends packet */
	SendPacket();

	/* Flush serial input buffer */
	FlushCommandSerial();

	/* Waits for response */
	if (ReceivePacketCommand(&id, &command, &bufferPayload[0], &payloadSize, LoRaMESH::defaultTimeout) != MESH_OK)
		return MESH_ERROR;

	/* Checks command */
	if (command != CMD_LORACLASS)
		return MESH_ERROR;

	readClassInfo->loraClass = bufferPayload[1];
	readClassInfo->window = bufferPayload[2];

	if (id == deviceInfo.id) /* Local read */
		classInfo = *readClassInfo;
	else 
		remoteClassInfo = *readClassInfo;

	return MESH_OK;
}

String LoRaMESH::GetLocalDeviceInfoString(bool compact) 
{
	return ConvertLoRaMESHInfoToString(deviceInfo, compact);
}

String LoRaMESH::GetLocalDeviceModInfoString(bool compact)
{
	return ConvertLoRaMESHModInfoToString(modInfo, compact);
}

String LoRaMESH::GetLocalDeviceClassInfoString(bool compact) 
{
	return ConvertLoRaMESHClassInfoToString(classInfo, compact);
}

String LoRaMESH::GetRemoteDeviceInfoString(bool compact)
{
	return ConvertLoRaMESHInfoToString(remoteDeviceInfo, compact);
}

String LoRaMESH::GetRemoteDeviceModInfoString(bool compact)
{
	return ConvertLoRaMESHModInfoToString(remoteModInfo, compact);
}

String LoRaMESH::GetRemoteDeviceClassInfoString(bool compact)
{
	return ConvertLoRaMESHClassInfoToString(remoteClassInfo, compact);
}

void LoRaMESH::ListenCommand()
{
	#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialCommand->isSoftwareSerial() && !hSerialCommand->thisSoftwareSerial->isListening())
		hSerialCommand->thisSoftwareSerial->listen();
	#endif
}

void LoRaMESH::ListenTransparent()
{
	#ifndef SOME_SERIAL_NOT_SUPPORT_SOFTWARE_SERIAL
	if (hSerialTransparent->isSoftwareSerial() && !hSerialTransparent->thisSoftwareSerial->isListening())
		hSerialTransparent->thisSoftwareSerial->listen();
	#endif
}

uint16_t LoRaMESH::ComputeCRC(uint8_t *data_in, uint16_t length)
{
	uint16_t i;
	uint8_t bitbang, j;
	uint16_t crc_calc;

	crc_calc = 0xC181;
	for (i = 0; i < length; i++)
	{
		crc_calc ^= (((uint16_t)data_in[i]) & 0x00FF);

		for (j = 0; j < 8; j++)
		{
			bitbang = crc_calc;
			crc_calc >>= 1;

			if (bitbang & 1)
			{
				crc_calc ^= 0xA001;
			}
		}
	}

	return (crc_calc & 0xFFFF);
}

uint16_t LoRaMESH::ComputeCRCBusModule(uint8_t *data_in, uint16_t length)
{
	uint16_t i;
	uint8_t id_l, id_h;
	uint8_t bitbang, j;
	uint16_t crc_calc;

	crc_calc = 0xC181;
	id_l = deviceInfo.id & 0xFF;
	id_h = (deviceInfo.id >> 8) & 0x03;
	// incluindo manualmente o deviceId no início do cálculo:
	crc_calc ^= (((uint16_t)id_l) & 0x00FF);
	for (j = 0; j < 8; j++)
	{
		bitbang = crc_calc;
		crc_calc >>= 1;

		if (bitbang & 1)
		{
			crc_calc ^= 0xA001;
		}
	}

	crc_calc ^= (((uint16_t)id_h) & 0x00FF);
	for (j = 0; j < 8; j++)
	{
		bitbang = crc_calc;
		crc_calc >>= 1;

		if (bitbang & 1)
		{
			crc_calc ^= 0xA001;
		}
	}

	for (i = 0; i < length; i++)
	{
		crc_calc ^= (((uint16_t)data_in[i]) & 0x00FF);

		for (j = 0; j < 8; j++)
		{
			bitbang = crc_calc;
			crc_calc >>= 1;

			if (bitbang & 1)
			{
				crc_calc ^= 0xA001;
			}
		}
	}
	return (crc_calc & 0xFFFF);
}

MeshStatus_Typedef LoRaMESH::PrepareFrameTranspBusModule(uint16_t id, uint8_t *payload, uint8_t payloadSize)
{
	uint8_t i = 0;
	uint16_t crc = 0;

	frame.size = payloadSize + 2; // Incluindo cálculo do CRC no protocolo de dados do BusModule

	if (payload == NULL)
		return MESH_ERROR;
	if (id > 1023)
		return MESH_ERROR;
	// if (IsUndefined()) return MESH_ERROR;

	if ((id != 0) && IsMaster()) /* Is master */
	{
		frame.size = frame.size + 2;
		/* Loads the target's ID */
		frame.buffer[i++] = id & 0xFF;
		frame.buffer[i++] = (id >> 8) & 0x03;
	}

	if ((payloadSize >= 0) && (payloadSize < MAX_PAYLOAD_SIZE))
	{
		/* Loads the payload */
		memcpy(&frame.buffer[i], payload, payloadSize);
		/* Computes CRC */
		if ((id != 0) && IsMaster()) /* Se for mestre */
		{								  // Calcula o CRC de modo normal pq o id do destino vai na mensagem
			crc = ComputeCRC((&frame.buffer[0]), payloadSize + 2);
			frame.buffer[payloadSize + 2] = crc & 0xFF;
			frame.buffer[payloadSize + 3] = (crc >> 8) & 0xFF;
		}
		else
		{ // Se não for mesmo, mesmo assim vamos incluir o deviceId no cálculo do CRC
			// Pois esse valor será necessário no mestre para identificar o pacote completo.
			crc = ComputeCRCBusModule((&frame.buffer[0]), payloadSize);
			frame.buffer[payloadSize + 0] = crc & 0xFF;
			frame.buffer[payloadSize + 1] = (crc >> 8) & 0xFF;
		}
	}
	else
	{
		/* Invalid payload size */
		memset(&frame.buffer[0], 0, MAX_BUFFER_SIZE);
		return MESH_ERROR;
	}

	frame.command = false;

	return MESH_OK;
}